#!/usr/bin/python
import urllib, urllib2, cookielib
from bs4 import BeautifulSoup
import ConfigParser, os

class MacToIpTools:
    # get the absolute path of the working directory of the script.
    SCRIPT_WORKING_DIRECTORY = os.path.dirname(os.path.realpath(__file__))
    ROUTER_AUTHENTICATION_SECTION = "ROUTER_AUTHENTICATION"
    IGNORED_DEVICES_SECTION = "DEVICES_TO_IGNORE"
    EXEMPT_DEVICES={}
    # Path to the config file is the working directory of the script + the name of the config file.
    CONFIG_FILE_PATH = os.path.join(SCRIPT_WORKING_DIRECTORY,'config.cfg')
    
    def __init__(self):
        if os.path.isfile(self.CONFIG_FILE_PATH) == False:
            print "Please copy example.cfg to config.cfg and change its options according to your needs"
            exit()

        config = ConfigParser.ConfigParser()
        config.read(self.CONFIG_FILE_PATH)
        self.USERNAME = config.get(self.ROUTER_AUTHENTICATION_SECTION, 'USERNAME')
        self.PASSWORD = config.get(self.ROUTER_AUTHENTICATION_SECTION, 'PASSWORD')
        
        options = config.options(self.IGNORED_DEVICES_SECTION)
        ignored_devices={}
        for op in options:
            ignored_devices[config.get(self.IGNORED_DEVICES_SECTION,op)]=op

        self.EXEMPT_DEVICES = ignored_devices
        print("Ignored devices, if you choose to ignore them, are as follows:")
        print(self.EXEMPT_DEVICES)
        print('\n')

    def getPageFromRouter(self):
        username = self.USERNAME
        password = self.PASSWORD
        url = 'http://182.168.1.1/DHCPTable.htm'
        p = urllib2.HTTPPasswordMgrWithDefaultRealm()
        p.add_password(None, url, username, password)
        handler = urllib2.HTTPBasicAuthHandler(p)
        opener = urllib2.build_opener(handler)
        urllib2.install_opener(opener)
        page = urllib2.urlopen(url).read()
        #print page
        s = BeautifulSoup(page)
        return s

    def getDeviceListFromPage(self,soup):
        devices = soup.findAll("tr", bgcolor="cccccc")
        devList ={}
        for d in devices:
            columns = d.findAll("td")
            # ip is always the second column in this table. Extra space at the end.
            ip = columns[1].text.encode("UTF-8").strip()
            
            # mac is always the third column in this table. Extra space at the end.
            mac = columns[2].text.encode("UTF-8").strip()

            devList[mac]=ip
        return devList

    def getIpsToIgnore(self):
        soup = self.getPageFromRouter()
        devices = self.getDeviceListFromPage(soup)

        ips_to_ignore=[]
        for exempt_d in self.EXEMPT_DEVICES.keys():
            if(exempt_d in devices):
                ips_to_ignore.append(devices[exempt_d])

        return ips_to_ignore

    def getFormattedIpsToIgnore(self):
        ips = self.getIpsToIgnore()
        return ','.join(ips)
