#!/bin/bash
#the purpose of this script is to cut off net for the user pi when other computers are on the network and restore it when it's alone
SLEEP_TIME_SECS=10
net_off="/home/pi/net_off"
net_on="/home/pi/net_on"
user_to_block=rtorrent
test=0
IGNORE_ME=$1
echo "Ignoring me? $1"
#sleep 30

function anyoneOnNetwork()
{
	if [ "$IGNORE_ME" = "1" ];
	then
		SCAN_RESULT=$(nmap -sn 182.168.1.100-103 182.168.1.106-110) 
	else
		SCAN_RESULT=$(nmap -sn 182.168.1.100-104 182.168.1.106-110) 
	fi
	FOUND=$(grep -cim1 "Host is up" <<< "$SCAN_RESULT")
	echo $FOUND
}

function init()
{
	#clear the iptables to establish an inital state
	iptables -F
	rm $net_off
	touch $net_on
}

$(init)
while :
do
#	V=$(anyoneOnNetwork)
#	echo "result is"
#	echo "$V"
#	exit
	if [ $(anyoneOnNetwork) == "0" ]
	then
		if [ -e $net_off ];
		then
		echo "all quiet" $(date)
		sudo iptables -F
		rm $net_off
		touch $net_on
		fi
	elif [ -e $net_on ]
	then
		echo "somebody's out there!" $(date)
		touch /home/pi/shutdown_at
		#Sync so we don't lose data
		#/sbin/shutdown -h now

		if [ $test == "0" ]
		then
			#sync
			sudo iptables -A OUTPUT -m owner --uid-owner $user_to_block -j REJECT
		else
			echo test
		fi

		# --reject-with icmp-port-unreachable
		rm $net_on
		touch $net_off
	fi
	sleep $SLEEP_TIME_SECS
done
