#!/usr/bin/python
import subprocess
import argparse
import time
from m2ip import MacToIpTools
mipTools = MacToIpTools()

parser = argparse.ArgumentParser(description='Watch for other computers on the network, suspend downloads when found')
parser.add_argument("-i","--ignore-self",action='store_true' ,help="whether you should be ignored. 'you' is determined by a constant ip")
parser.add_argument("-l","--log",action='store_true' ,help="Whether to keep logs of each request, this is likely to be large.")
parser.add_argument("-v","--verbose",action="store_true", help="Prints the results of each scan")
args = parser.parse_args()

user_to_block='rtorrent'
LOG_LOCATION='/dev/shm/log.txt'
SLEEP_SECONDS=10
BASE_SCAN_COMMAND="nmap -n -sn 182.168.1.100-110 -oG - --max-rtt-timeout 1500ms"
BLOCK_COMMAND="iptables -A OUTPUT -m owner --uid-owner {} -j REJECT".format(user_to_block)
CLEAR_COMMAND="iptables -F"
t=""
PREV_BLOCKED=False

def init():
    subprocess.call(CLEAR_COMMAND.split(' '))

def appendExcludedIpParam(command):
    command.append("--exclude")
    command.append(mipTools.getFormattedIpsToIgnore())
    return command

def computersDetected(count):
    global PREV_BLOCKED
    detected = count > 0
    if(detected and not PREV_BLOCKED):
        print("computers online: {} at {}".format(count,time.strftime('%c')))
        subprocess.call(BLOCK_COMMAND.split(' '))
        PREV_BLOCKED = True
        if(args.log):
            writeLog(t)
    elif(detected == False and PREV_BLOCKED):
        print("all clear at {}".format(time.strftime('%c')))
        subprocess.call(CLEAR_COMMAND.split(' '))
        PREV_BLOCKED = False
        if(args.log):
            writeLog(t)

def writeLog(t):
    f = open(LOG_LOCATION,'a+')
    f.write(t+'\n')
    f.close()

init()
if(args.ignore_self):
    print("Ignoring me")
else:
    print("Not ignoring me")

while(True):
    c = BASE_SCAN_COMMAND.split(' ')
    if(args.ignore_self):
        c = appendExcludedIpParam(c)
    t=subprocess.check_output(c)
    count = t.count('Status: Up')
    if(args.verbose):
        print(t)
        print('Count is: ' + str(count))
    computersDetected(count)
    #if( count > 0 ):
    #    computersDetected(True)
    #else:
    #    print("offline")
    #    computersDetected(False)
    time.sleep(SLEEP_SECONDS)

